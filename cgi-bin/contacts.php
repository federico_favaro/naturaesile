<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../photos/marchio_topo.png">

    <title>Contattaci</title>

    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../carousel.css" rel="stylesheet"> 
    <link href="../signin.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="../index.html">Natura&Sile</a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li><a href="../index.html">Home</a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Chi siamo <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="../who.html">Team</a></li>
                    <li><a href="../wip.html">portfolio & Curriculum</a></li>
                    <li class = "active"><a href="contacts.php">Contatti</a></li>
                  </ul>
                </li>
                  <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Foto<b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="../wip.html">Fiume Sile</a></li>
                    <li><a href="../wip.html">Treviso & Dintorni</a></li>
                    <li><a href="../wip.html">Natura</a></li>
                  </ul>
                </li>
                <li><a href="../wip.html">Pubblicazioni</a></li>
                <li><a href="../wip.html">News & Events</a></li>
              </ul>
            </div>
          </div>
        </div>

      </div>
    </div>

    <div class="container">

      <form class="form-signin"  action="submit.php" method="post">
        <h2 class="form-signin-heading">Contattaci</h2>
        <input type="text" name="nome" class="form-control" placeholder="Nome" required>
        <input type="email" name="mail" class="form-control" placeholder="e-mail" required>
        <textarea class="form-control" name="msg" rows="10" placeholder="Messaggio"> </textarea>
        <button class="btn btn-lg btn-primary btn-block" type="Invia">Invia</button>
        <?
            if(isset($_GET["ok"])) 
            {
              $html_string = "<div class=\"alert alert-success alert-dismissable\">";
              $html_string .= "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
              $html_string .= "<strong>Yeah!</strong> Il Tuo messaggio è stato spedito correttamente!";
              $html_string .= "</div>";
              echo $html_string;

            }
            if(isset($_GET["error"])) 
            {
              $html_string = "<div class=\"alert alert-danger alert-dismissable\">";
              $html_string .= "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
              $html_string .= "<strong>No!!!</strong> Ci sono stati dei problemi! Riprova!";
              $html_string .= "</div>";
              echo $html_string;

            }
        ?>
      </form>
            <footer>
   <!--     <p class="pull-right"><a href="#">Back to top</a></p> -->
        <p>&copy; 2014 Natura&Sile &middot; <a href="mailto:photo@naturaesile.it">photo@naturaesile.it</a>
      </footer>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
        <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="../dist/js/bootstrap.min.js"></script>
    <script src="../docs-assets/js/holder.js"></script>
  </body>
</html>